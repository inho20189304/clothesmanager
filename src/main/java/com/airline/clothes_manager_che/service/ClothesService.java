package com.airline.clothes_manager_che.service;

import com.airline.clothes_manager_che.entity.Clothes;
import com.airline.clothes_manager_che.repository.ClothesRepository;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ClothesService {
    private final ClothesRepository clothesRepository;

    public void setClothes(String clothesBrand, String clothesType, String clothesColor, String clothesOwner) {
        Clothes addData = new Clothes();
        addData.setClothesBrand(clothesBrand);
        addData.setClothesType(clothesType);
        addData.setClothesColor(clothesColor);
        addData.setClothesOwner(clothesOwner);

        clothesRepository.save(addData);

    }


}
