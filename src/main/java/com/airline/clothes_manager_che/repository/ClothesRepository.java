package com.airline.clothes_manager_che.repository;

import com.airline.clothes_manager_che.entity.Clothes;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ClothesRepository extends JpaRepository<Clothes, Long> {
}
