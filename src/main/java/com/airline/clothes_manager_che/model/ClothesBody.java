package com.airline.clothes_manager_che.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClothesBody {

    private String clothesBrand;
    private String clothesType;
    private String clothesColor;

    private String clothesOwner;



}
