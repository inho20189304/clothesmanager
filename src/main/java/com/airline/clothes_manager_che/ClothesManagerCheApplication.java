package com.airline.clothes_manager_che;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClothesManagerCheApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClothesManagerCheApplication.class, args);
    }

}
