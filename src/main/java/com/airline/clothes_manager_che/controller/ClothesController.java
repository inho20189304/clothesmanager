package com.airline.clothes_manager_che.controller;

import com.airline.clothes_manager_che.model.ClothesBody;
import com.airline.clothes_manager_che.repository.ClothesRepository;
import com.airline.clothes_manager_che.service.ClothesService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/clothes")
public class ClothesController {
    private final ClothesService clothesService;

    @PostMapping("/data")
    public String setClothes(@RequestBody ClothesBody body) {
        clothesService.setClothes(body.getClothesBrand(), body.getClothesType(), body.getClothesColor(), body.getClothesOwner());

        return "ok";
    }
}
